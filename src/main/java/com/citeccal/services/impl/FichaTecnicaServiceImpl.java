package com.citeccal.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.citeccal.commons.GenericServiceImpl;
import com.citeccal.dao.api.FichaTecnicaDaoAPI;
import com.citeccal.models.FichaTecnica;
import com.citeccal.services.api.FichaTecnicaServiceAPI;

@Service
public class FichaTecnicaServiceImpl extends GenericServiceImpl<FichaTecnica, Long> implements FichaTecnicaServiceAPI {
	@Autowired
	private FichaTecnicaDaoAPI fichaTecnicaDaoAPI;
	
	@Override
	public CrudRepository<FichaTecnica, Long> getDao() {
		return fichaTecnicaDaoAPI;
	}

}