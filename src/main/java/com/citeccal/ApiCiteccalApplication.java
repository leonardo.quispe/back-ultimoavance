package com.citeccal;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

/*import java.io.File;*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.citeccal.dao.api.UserDetailsRepository;
import com.citeccal.models.Authority;
import com.citeccal.models.User;


@SpringBootApplication
public class ApiCiteccalApplication {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserDetailsRepository userDetailsRepository;

	public static void main(String[] args) {
		
		SpringApplication.run(ApiCiteccalApplication.class, args);
	}
	
	
	@PostConstruct
	protected void init() {
		
		//if(userDetailsRepository.findById((long) 1) == null) {
			List<Authority> authorityList=new ArrayList<>();
			List<Authority> authorityList2=new ArrayList<>();
			
			authorityList2.add(createAuthority("USER","User role"));
			authorityList.add(createAuthority("ADMIN","Admin role"));
			
			User user=new User();
			User user2 = new User();
			user2.setUsername("user");
			user.setUsername("administrador");
			
			user2.setPassword(passwordEncoder.encode("User@123"));
			user2.setEnabled(true);
			user2.setAuthorities(authorityList2);
			user.setPassword(passwordEncoder.encode("Admin@123"));
			user.setEnabled(true);
			user.setAuthorities(authorityList);
			
			userDetailsRepository.save(user2);
			userDetailsRepository.save(user);
		//}else {
		//	return;
		//}
	}
	
	
	private Authority createAuthority(String roleCode,String roleDescription) {
		Authority authority=new Authority();
		authority.setRoleCode(roleCode);
		authority.setRoleDescription(roleDescription);
		return authority;
	}
	
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("http://localhost:3000").allowedMethods("*").allowedHeaders("*");
			}
		};
	}


}