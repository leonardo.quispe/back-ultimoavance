package com.citeccal.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.citeccal.dao.api.UserDetailsRepository;
import com.citeccal.models.User;

@Service
public class CustomUserService implements UserDetailsService{
	
	@Autowired
	UserDetailsRepository UserDetailsRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = UserDetailsRepository.findByUsername(username);
		
		if (null == user) {
			throw new UsernameNotFoundException("User Not Found with username "+username);
		}
		return user;
	}

}
