package com.citeccal.controllers;

import java.io.IOException;
/*import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;*/
import java.util.List;
import javax.persistence.Entity;
import javax.servlet.http.HttpServletRequest;
/*import org.apache.tomcat.jni.File;*/
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
/*import org.springframework.web.bind.annotation.RequestParam;*/
import org.springframework.web.bind.annotation.RestController;
/*import org.springframework.web.multipart.MultipartFile;*/
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.citeccal.imgfiles.FileStorageService;
import com.citeccal.models.FichaTecnica;
import com.citeccal.services.api.FichaTecnicaServiceAPI;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/ficha/")
public class FichaTecnicaController {
	
	@Autowired
	private FichaTecnicaServiceAPI fichaTecnicaServiceAPI;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@GetMapping(value = "/all")
	public List<FichaTecnica> getAll() {
		return fichaTecnicaServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public FichaTecnica find(@PathVariable Long id) {
		return fichaTecnicaServiceAPI.get(id);
	}
	/*public static String uploadDirectory = System.getProperty("user.dir") + "/src/main/webapp/imagedata";*/
	
	@PostMapping(value = "/save", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<FichaTecnica> save(@RequestPart("fichaTecnica") FichaTecnica fichaTecnica, @RequestPart("file") MultipartFile file
			/*@RequestParam("file2") MultipartFile file2,
			@RequestParam("file3") MultipartFile file3,
			@RequestParam("file4") MultipartFile file4,*/) throws IOException {
		FichaTecnica obj = new FichaTecnica();
		ObjectMapper map = new ObjectMapper();
		Entity fichaTecnicas = map.reader()
				.forType(new TypeReference<List<FichaTecnica>>() {})
				.readValue(fichaTecnica);
		String fileName = fileStorageService.storeFile(file);
		/*String fileName2 = fileStorageService.storeFile(file2);
		String fileName3 = fileStorageService.storeFile(file3);
		String fileName4 = fileStorageService.storeFile(file4);*/
		
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/api/ficha/")
				.path(fileName)
				.toUriString();
		/*String fileDownloadUri2 = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/files/")
				.path(fileName2)
				.toUriString();
		String fileDownloadUri3 = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/files/")
				.path(fileName3)
				.toUriString();
		String fileDownloadUri4 = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/files/")
				.path(fileName4)
				.toUriString();*/
		obj = fichaTecnicaServiceAPI.save(fichaTecnicas);
		obj.setImgRuta1(fileDownloadUri);
		/*obj.setImgRuta2(fileDownloadUri2);
		obj.setImgRuta3(fileDownloadUri3);
		obj.setImgRuta4(fileDownloadUri4);*/
		return new ResponseEntity<FichaTecnica>(obj, HttpStatus.OK);
	}
	/*@PostMapping(value = "/save")
	public ResponseEntity<FichaTecnica> save(@RequestBody FichaTecnica fichaTecnica, @RequestParam("img") MultipartFile file ) {
		
		StringBuilder fileNames = new StringBuilder();
		String filename = fichaTecnica.getId() + file.getOriginalFilename().substring(file.getOriginalFilename().length()-4);
		String url =  "http://192.168.1.12:8080" + filename;
		Path fileNameAndPath =Paths.get(uploadDirectory,url);
		
		try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fichaTecnica.setImgRuta1(url);
		fichaTecnica.setImgRuta2(filename);
		fichaTecnica.setImgRuta3(filename);
		fichaTecnica.setImgRuta4(filename);
		
		FichaTecnica obj = fichaTecnicaServiceAPI.save(fichaTecnica);
		return new ResponseEntity<FichaTecnica>(obj, HttpStatus.OK);
	}*/
	
	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<FichaTecnica> delete(@PathVariable Long id) {
		FichaTecnica fichaTecnica = fichaTecnicaServiceAPI.get(id);
		if (fichaTecnica != null) {
			fichaTecnicaServiceAPI.delete(id);
		} else {
			return new ResponseEntity<FichaTecnica>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<FichaTecnica>(fichaTecnica, HttpStatus.OK);
	}
	
	@GetMapping(value= "/{fileName}")
	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName,HttpServletRequest request){
		
		Resource resource = fileStorageService.loadFileAsResource(fileName);
		
		String contentType = null;
		
		try {
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
		}catch(IOException ex) {
			System.out.println("Could not determine fileType");
		}
		
		if(contentType==null) {
			contentType = "application/octet-stream";
		}
		
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(contentType))
				.body(resource);
	}

}