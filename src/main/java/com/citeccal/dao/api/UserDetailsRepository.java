package com.citeccal.dao.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.citeccal.models.User;

@Repository
public interface UserDetailsRepository extends JpaRepository<User, Long>{
	
	User findByUsername(String username);
}
