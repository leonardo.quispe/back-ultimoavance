package com.citeccal.dao.api;

import org.springframework.data.repository.CrudRepository;
import com.citeccal.models.FichaTecnica;

public interface FichaTecnicaDaoAPI extends CrudRepository<FichaTecnica, Long> {

}